'use strict';

//= template/modernizr-3.11.2.min.js
//= template/jquery-3.6.0.min.js
//= template/jquery-migrate-3.3.2.min.js
//= template/popper.min.js
//= template/bootstrap.min.js
//= template/select2.min.js
//= template/jquery.nice-select.min.js
//= template/wow.js
//= template/scrollup.js
//= template/swiper.min.js
//= template/waypoints.min.js
//= template/counterup.js
//= template/smoothscroll.js
//= template/mouse-parallax.js
//= template/jarallax.min.js
//= template/slinky.min.js
//= template/easyzoom.js
//= template/magnific-popup.js
//= template/images-loaded.js
//= template/isotope.js
//= template/jquery-ui.js
//= template/jquery-ui-touch-punch.js
//= template/jquery.mb.ytplayer.min.js
//= template/ajax-mail.js

//= ../node_modules/lazysizes/lazysizes.js


$(document).ready(function () {

});




/*
9.
10.
11.
12.
13. Сделал
14. Сделал. Сейчас выводится для админа один контент, а для юзеров другой чтобы проверили внешний вид.
15. Сделал. Есть вопросы. Что такое "лид форма" и что на банере должно быть.
16. Сделал.
18. Сделал. На активный инпут накинул тень.
21. 
22.
24. Сделал. Заменил у всех кнопок, схожих с стр, которую ты скину. Пусть проверит подойдет так или нет





остальные работы:
9. Заменить фото на главной на новые хайрезы (предоставим) 
- В каком месте менять и где картинка?
10.Заменить достижения с картинок на иконки, сделать кликабельными
- А какие иконки использовать?
11. Сделать кликабельными преимущества. Можно заменить на схожие элементы из библиотеки иконок, но с анимацией?
- На какие иконки менять? Внешний вид остается прежним? А какую анимацию? Если вид останется прежним, а изменятся только картинки внутри, то моргу придумать простенькую анимацию.
12. Переработать блок с каруселью внизу, чтобы избежать обрезки картинок 
- Это невозможно или возможно, но потребуется много времени на реализацию. Этот блок всегда динамически меняет свою ширину(он на весь экран) и ни один слайдер не умеет так делать.
- Предлагаю ограниить показ слайдов в 1 или 2шт.
15 .Переработать страницы отдельных услуг, привести к формату: 
- Как я понял, нужно добавить вверху баннерную плашку
- а все проекты скрыть под кнопку и показывтаь только при клике?
16. В продукции ссылку на презентацию заменить на кнопку. Добавить форму "Оставить заявку"
- перенести кнопку "презентация" как показана на картинке?
- Форма делать модалкой или прям на страницу вставить?

21. Страницу Генпроектирование предлагается оформить как лендинг по шаблону. 
- не понял. По какому шаблону?
22. Для портфолио сделать шаблон типовой страницы, чтобы мы не заполняли каждый раз заново. 
- Это для бекаенда наверно, но не уверен.
24.Попробовать кнопки на белом, которые закрашены бордовым цветом, перекрашивать ярко красным (есть на главной странице) цветом 
- Не понятно.

остальные работы:
1. Сделал. 1300 и > экран шрифт увеличен
2. Сделал
5. Сделал( Развести формы в мобильной версии, сейчас они в куче)
6. Сделал
8. Сделал
9. Заменить фото на главной на новые хайрезы (предоставим) 
- В каком месте менять и где картинка?
10.Заменить достижения с картинок на иконки, сделать кликабельными
- А какие иконки использовать?
11. Сделать кликабельными преимущества. Можно заменить на схожие элементы из библиотеки иконок, но с анимацией?
- На какие иконки менять? Внешний вид остается прежним? А какую анимацию? Если вид останется прежним, а изменятся только картинки внутри, то моргу придумать простенькую анимацию.
12. Переработать блок с каруселью внизу, чтобы избежать обрезки картинок 
- Это невозможно или возможно, но потребуется много времени на реализацию. Этот блок всегда динамически меняет свою ширину(он на весь экран) и ни один слайдер не умеет так делать.
- Предлагаю ограниить показ слайдов в 1 или 2шт.
13. Подвал - текст о компании и содержимое контактов рекомендуем сделать одним шрифтом. Также телефон и почту выделить цветом как гиперссылку. Добавить форму обратной связи. 
- Текст одним шрифтом и был
- у телефона сделал выделение на hover
- а какого вида форма должна быть и куда вставить?
14. Упорядочить стиль каталога на единый - блоки и картинки единого размера и стиля. Формат нужно уточнить.
- Нужны пояснения
15 .Переработать страницы отдельных услуг, привести к формату: 
- Как я понял, нужно добавить вверху баннерную плашку
- а все проекты скрыть под кнопку и показывтаь только при клике?
16. В продукции ссылку на презентацию заменить на кнопку. Добавить форму "Оставить заявку"
- перенести кнопку "презентация" как показана на картинке?
- Форма делать модалкой или прям на страницу вставить?
17. Сделал
18.
19. Сделал
20. Сделал
21.
22.
23. Сделал
24.
*/
